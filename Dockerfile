#pull base image
FROM openjdk:13-jdk-alpine

#expose port 8080
EXPOSE 8080

#default command
CMD java -jar /data/token-1.1.jar

#copy demo to docker image
ADD ./target/token-1.1.jar /data/token-1.1.jar