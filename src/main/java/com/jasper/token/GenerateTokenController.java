package com.jasper.token;

//import com.jaspersoft.cipher.myCipher;

import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

@RestController
public class GenerateTokenController {

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public String create(@RequestBody Map<String, String> body) {
        String user = body.get("user");
        myCipher obj = new myCipher();
        String expired = obj.getExpired();
        String encryptString = "u=" + user + "|exp=" + expired;
        return obj.encrypt(encryptString);
    }
}
