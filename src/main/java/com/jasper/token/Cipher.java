package com.jasper.token;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.jaspersoft.jasperserver.api.common.crypto.CipherI;

class myCipher implements CipherI {

    private SecretKeySpec secretKey;
    private byte[] key;
    String myKey = "Saranros playwork";


    private void setKey(String myKey) {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "RC4");

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public String decrypt(String strToDecrypt) {

        try {
            String secret = myKey;
            setKey(secret);
            Cipher cipher = Cipher.getInstance("RC4");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getUrlDecoder().decode(strToDecrypt.getBytes("UTF-8"))));

        } catch (Exception e) {
            System.out.println("Error while decrypting !" + e.toString());
            System.out.println("Error while descrypt GetMEssage()  : " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public String encrypt(String strToEncrypt) {

        try {
            String secret = myKey;
            setKey(secret);
            Cipher cipher = Cipher.getInstance("RC4");
            System.out.println("encrypt secret  " + secretKey);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getUrlEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));


        } catch (Exception e) {
            System.out.println("Error while encrypting" + e.toString());
        }
        return null;

    }

    public String getExpired() {
        try {
            Date myDate = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.HOUR_OF_DAY, 24); // this will add two hours
            myDate = cal.getTime();
            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssZ");
            return df.format(myDate);
        } catch (Exception ignored) {

        }
        return null;
    }


}
