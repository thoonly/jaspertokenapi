package com.jasper.token;

//import com.jaspersoft.cipher.myCipher;

import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RestController
public class RefreshTokenController {

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public String create(@RequestBody Map<String, String> body) {
        myCipher obj = new myCipher();
        String expired = obj.getExpired();;
        String tokenString = body.get("token");
        String decryptString = obj.decrypt(tokenString);
        String user = decryptString.split("\\|")[0];
        String encryptString = "u=" + user.substring(2) + "|exp=" + expired;
        return obj.encrypt(encryptString);
    }

}
